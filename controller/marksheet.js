let student_details = [
    {
        studentName: 'Varsha',
        studentID: 1,
        subject1: 23,
        subject2: 34,
        subject3: 35,
        subject4: 36,
        subject5: 37
    },
    {
        studentName: 'Resh',
        studentID: 2,
        subject1: 23,
        subject2: 34,
        subject3: 35,
        subject4: 36,
        subject5: 37
    }
]

// Handlers
const getAllDetails = async (req, reply) => {
    return student_details
}


const addDetails = async (req, reply) => {
    const studentID = Number(req.body.studentID)
    const studentName = String(req.body.studentName)
    const subject1 = Number(req.body.subject1)
    const subject2 = Number(req.body.subject2)
    const subject3 = Number(req.body.subject3)
    const subject4 = Number(req.body.subject4)
    const subject5 = Number(req.body.subject5)

    const newStudent = {
        studentName,
        studentID,
        subject1,
        subject2,
        subject3,
        subject4,
        subject5
    }

    student_details.push(newStudent)
    return { msg: `New student details added for ID ${studentID}` }
}

const updateMarks = async (req, reply) => {
    const id = Number(req.body.studentID)
    var subject_name
    var sub_mark
    if (req.body.subject1){
        subject_name = 'subject1'
        sub_mark = Number(req.body.subject1)
    }
    else if(req.body.subject2){
        subject_name = 'subject2'
        sub_mark = Number(req.body.subject2)
    }
    else if(req.body.subject3){
        subject_name = 'subject3'
        sub_mark = Number(req.body.subject3)
    }
    else if(req.body.subject4){
        subject_name = 'subject4'
        sub_mark = Number(req.body.subject4)
    }
    else if(req.body.subject5){
        subject_name = 'subject5'
        sub_mark = Number(req.body.subject5)
    }
    student_details.forEach(element => {if (element.studentID == id){
    element[subject_name] = sub_mark}})
    
    return { msg: `Mark updated for ID ${id}` }
    
}

const deleteDetails = async (req, reply) => {
    const id = Number(req.body.studentID)

    student_details = student_details.filter(student_details => student_details.studentID !== id)
    return { msg: `Student details with ID ${id} is deleted` }
}

module.exports = {
    getAllDetails,
    addDetails,
    updateMarks,
    deleteDetails
}