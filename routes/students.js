const studentController = require('../controller/marksheet');

const routes = [{
        method: 'GET',
        url: '/report',
        handler: studentController.getAllDetails
    },
    {
        method: 'POST',
        url: '/add',
        handler: studentController.addDetails
    },
    {
        method: 'POST',
        url: '/update',
        handler: studentController.updateMarks
    },
    {
        method: 'DELETE',
        url: '/delete',
        handler: studentController.deleteDetails
    }
]



module.exports = routes